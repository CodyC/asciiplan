var ThemeService = {
	SAVED_THEME_KEY: "SelectedTheme",
	
	SetupTheme: function(){
		
		var selectedTheme = localStorage.getItem("SelectedTheme")
		
		var GREEN_THEME_ID = "greenHighlight";
		var BLUE_THEME_ID = "blueHighlight";
		var YELLOW_THEME_ID = "yellowHighlight";
		
		var THEME_IDS = 
			[GREEN_THEME_ID, BLUE_THEME_ID, YELLOW_THEME_ID];

		//jQuery's .removeClass( [className] ):className
		//Can remove one or more space-separated classes
		var THEME_IDS_REMOVAL_STRING = THEME_IDS.join(" ");

		var jQueryIdsToTheme
			= ['input', 
			   '#ExampleShapeTxt', 
			   '#ShapeSelect', 
			   '#ShapeConfigurationDialog', 
			   '#AboutDiv'];
		
		$.each(jQueryIdsToTheme, 
			function(currentIndex){
				$(jQueryIdsToTheme[currentIndex])
					.removeClass(THEME_IDS_REMOVAL_STRING);
			});
		
		var selectedThemeId =  "";
		
		switch (selectedTheme) {
		  case "Green":
		  default: // intentional fall through
			selectedThemeId = GREEN_THEME_ID;
			break;
			
		  case "Blue":
			selectedThemeId = BLUE_THEME_ID;
			break;
		  
		  case "Yellow":
			selectedThemeId = YELLOW_THEME_ID;
			break;
		}
		
		$.each(jQueryIdsToTheme, 
			function(currentIndex){
			
				$(jQueryIdsToTheme[currentIndex])
					.addClass(selectedThemeId);
			});
	},
	
	GetSavedTheme: function(){
		return localStorage.getItem(this.SAVED_THEME_KEY);
	},
	
	SaveTheme: function(valueToSave){
		localStorage.setItem(this.SAVED_THEME_KEY, valueToSave);
	},
	
};