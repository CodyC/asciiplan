var BrowserDetector = {

	IE_NAME: "IE",
	IE_USER_AGENT: "MSIE",
	FIREFOX_NAME: "Firefox",
	CHROME_NAME: "Chrome",
	OPERA_NAME: "Opera",
	OPERA_ABBREVIATION: "OPR",
	EDGE_NAME: "Edge",
	SAFARI_NAME: "Safari",
	
	GetBrowserName: function() { 

		var browserName = "";
		//Edge looks like a lot of other browsers through the user agent string
		//so just check to make sure it absolutely is only edge
		var containsEdge = navigator.userAgent.indexOf(this.EDGE_NAME) != -1;
		
		if((navigator.userAgent.indexOf(this.OPERA_NAME) || navigator.userAgent.indexOf(this.OPERA_ABBREVIATION)) != -1 ) 
		{
			browserName = this.OPERA_NAME;
		}
		else if(navigator.userAgent.indexOf(this.CHROME_NAME) != -1  && !containsEdge)
		{
			browserName = this.CHROME_NAME;
		}
		else if(navigator.userAgent.indexOf(this.SAFARI_NAME) != -1 && !containsEdge)
		{
			browserName = this.SAFARI_NAME;
		}
		else if(navigator.userAgent.indexOf(this.FIREFOX_NAME) != -1 && !containsEdge) 
		{
			browserName = this.FIREFOX_NAME;
		}
		else if((navigator.userAgent.indexOf(this.IE_USER_AGENT) != -1 ) || (!!document.documentMode == true ) 
					&& !containsEdge) //IF IE > 10
		{
			browserName = this.IE_NAME;
		}
		else if(containsEdge){
			browserName = this.EDGE_NAME;
		}
	
		return browserName;
	},
}