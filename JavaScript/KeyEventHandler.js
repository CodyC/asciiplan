
//Handles keyboard events
var KeyEventHandler = {
		
	isBackspaceKey: function(keyboardEvent){
		return keyboardEvent.keyCode == KeyCodes.BACKSPACE_KEY_CODE;
	},
	
	isLeftArrow: function(keyboardEvent){
		return keyboardEvent.keyCode == KeyCodes.LEFT_ARROW_KEY_CODE;
	},
	
	isRightArrow: function(keyboardEvent){
		return keyboardEvent.keyCode == KeyCodes.RIGHT_ARROW_KEY_CODE;
	},
	
	isUpArrow: function(keyboardEvent){
		return keyboardEvent.keyCode == KeyCodes.UP_ARROW_KEY_CODE;
	},
	
	isDownArrow: function(keyboardEvent){
		return keyboardEvent.keyCode == KeyCodes.DOWN_ARROW_KEY_CODE;
	},
	
	isTabKey: function(keyboardEvent){
		return keyboardEvent.keyCode == KeyCodes.TAB_KEY_CODE && !keyboardEvent.shiftKey;
	},
	
	isShiftTabKey: function(keyboardEvent){
		return keyboardEvent.keyCode == KeyCodes.TAB_KEY_CODE && keyboardEvent.shiftKey;
	},
	
	isTabOrShiftTab: function(keyboardEvent){
		return this.isTabKey(keyboardEvent) || this.isShiftTabKey(keyboardEvent);
	},
	
	isHomeKey: function(keyboardEvent){
		return keyboardEvent.keyCode == KeyCodes.HOME_KEY_CODE || keyboardEvent.which == KeyCodes.HOME_KEY_CODE;
	},
	
	isEndKey: function(keyboardEvent){
		return keyboardEvent.keyCode == KeyCodes.END_KEY_CODE || keyboardEvent.which == KeyCodes.END_KEY_CODE;
	},
	
	isHomeOrEndKey: function(keyboardEvent){
		return KeyEventHandler.isHomeKey(keyboardEvent) || KeyEventHandler.isEndKey(keyboardEvent);
	},
	
	isKeyboardPaste: function(keyboardEvent){
		return keyboardEvent.ctrlKey && keyboardEvent.keyCode == KeyCodes.V_KEY_CODE;
	},
	
	//Is the key a key reserved for advancement on the grid ???
	IsAdvancementKey: function(keyEvent){

		var isBackspaceKey = this.isBackspaceKey(keyEvent);
		var isLeftArrow = this.isLeftArrow(keyEvent);
		var isRightArrow = this.isRightArrow(keyEvent);
		var isUpArrow = this.isUpArrow(keyEvent);
		var isDownArrow = this.isDownArrow(keyEvent);
		var isTabKey = this.isTabKey(keyEvent);
		var isShiftTabKey = this.isShiftTabKey(keyEvent);
			
		var isAdvancementKey = 
			isBackspaceKey 
				|| isTabKey
				|| isShiftTabKey
				|| isDownArrow
				|| isUpArrow
				|| isLeftArrow
				|| isRightArrow;
					
		return isAdvancementKey;
	},
	
}