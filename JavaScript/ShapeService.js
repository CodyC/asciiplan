
var ShapeService = {

		DrawRectangle: function(height, width){
			var rectangleString = "";
			
			var lengthWiseLine = this.DrawVerticalLine(width);
			
			var sideCrossSection = "|";
			
			var spacesToPrint = width - 2;
			// this will give us the number of spaces to put between the 2 | on the cross section
		
			
			//Draw one cross section
			for(i=0; i < spacesToPrint; i++){
				sideCrossSection += " ";
			}
			
			sideCrossSection += "|";
			
			var sides = "\n";
			
			var crossSectionCount = height - 2;
			//This will give us the number of cross sections to put int
			//We have to subtract to for the top and bottom line
			
			//Add the requirerd amount of cross sections
			for(i=0; i <= crossSectionCount; i++){
				sides += sideCrossSection;
				sides += "\n";
			}
			
			rectangleString += lengthWiseLine;
			rectangleString += sides;
			rectangleString += lengthWiseLine;
			
			return rectangleString;
		},
		
		
		DrawVerticalLine: function(width){
			
			var lengthWiseLine = "";
			
			//Draw top/bottom line
			for(i=0; i < width; i++){
				lengthWiseLine += "-";
			}
			
			return lengthWiseLine;
		},

		DrawAngledLine: function(length, lineDirection){
			
			var angledLine = "";

			if(lineDirection === Directions.LEFT_TO_RIGHT){
			
				for(i=0; i <= length; i++){
				
					for(j=0; j <= i; j++){
						angledLine += " ";					
					}
					
					angledLine += "\\";
					
					angledLine += "\n";
				}
			} else if(lineDirection === Directions.RIGHT_TO_LEFT){
				
				for(i=0; i <= length; i++){
					
					var spacesToPrepend = length - i;
					for(j=0; j <= spacesToPrepend; j++){
						angledLine += " ";					
					}
					
					angledLine += "/";
					angledLine += "\n";
				}
			}
			
			return angledLine;
		},

		DrawTriangle: function(height){
			
			var triangleString = "";
			var leftSide = this.DrawAngledLine(height, Directions.RIGHT_TO_LEFT);
			
			//foreach line, put:
				//the correct number of spaces
				// a \
				//a \n (the split will kill it)
						
			var leftSideSplitArray = leftSide.split("\n");
			
			var spaceCounter = -1;
			
			for(i=0; i <= height; i++){
			
				var currentRow = leftSideSplitArray[i];
				
				for(j=0; j <= spaceCounter; j++){
					currentRow += " ";
				}
				
				spaceCounter += 2;
				
				currentRow += "\\";
				currentRow += "\n";
				
				triangleString += currentRow;
			}
			
			//draw the base
			var baseLength = (height * 2) + 2;
			var triangleBase = this.DrawVerticalLine(baseLength);
			
			//Add the a space for padding and the base to the triangle
			triangleString += " " + triangleBase;
			
			return triangleString;
		},
		
}
