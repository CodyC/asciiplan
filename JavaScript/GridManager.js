
//Expect Tron references throughout
var GridManager = {
	
	LastFocusedCellId: "",
	
	PrimaryGridDivToFilJQueryTag: "#parentGridDiv",
	
	//Adds Text to the grid.  Walks across cells and rows to ensure that one character per cell is added.
	//Returns the index of the the last cell that was written to.
	addTextToGrid: 
		function(textToAdd,
				  startX,
				  startY,
				  height,
				  width,
				  gridDivToFillJQueryTag){
		
		//Get all the inputs
		var inputs = this.getInputs(gridDivToFillJQueryTag);
		
		//Move to the correct coordinate
		var startIndex = (startX * height) + startY;
		var insertIndex = startIndex;
		
		var gridTotalArea = GridManager.GetGridTotalArea(height, width); 
		var characterCountToAdd = textToAdd.length;
		
		var lineCounter = 1;
		for (var i = 0; i < characterCountToAdd; i++) {
			
			console.log("insertIndex = " + insertIndex);
			
			var gridHasSpaceForCurrentCharacter = (insertIndex + i) <= gridTotalArea;
			
			console.log("gridHasSpaceForCurrentCharacter = " + gridHasSpaceForCurrentCharacter);
			
			if(gridHasSpaceForCurrentCharacter) {
			
				//Get the character to insert
				var currentCharacter = textToAdd.charAt(i);
				
				//Get the tag of the currently focused cell to insert the character into.
				var currentCellTag = this.getCellTagFromInput(inputs[insertIndex]);
				console.log("currentCellTag = " + currentCellTag);
				
				console.log("currentCharacter = " + currentCharacter);
				
				if(currentCharacter != Constants.NEW_LINE){
					insertIndex++; //Move the start index to the next cell
					$(currentCellTag).val(currentCharacter);
				} else {
					
					//If you hit a newline, 
						//Move the text down one row plus the current offset from 0x
					insertIndex = startIndex + (lineCounter * height);
					lineCounter++;
				}
				
			} else {
				//We can't paste anymore, just move on.
				break;
			}
		}
		
		return insertIndex;
	},
	
	fillGrid:
		function(divToFilJQueryTag,
				  height,
				  width,
				  cellNameSuffix){
		
		var htmlString = GridManager.getGridHTMLString(height, width, cellNameSuffix);
		
		$(divToFilJQueryTag).html(htmlString);
	},
	
	getGridHTMLString: function(height, width, cellNameSuffix){
		
		var htmlString = "";
		
		for(i=0; i<height; i++){
		
			var currentRowDivId = "y" + i + "Div";
			var rowDivStartTag = '<div id="' + currentRowDivId + '" class="gridDiv">'
		
			htmlString = htmlString.concat(rowDivStartTag);
		
			for(j=0; j<width; j++){
				
				var currentInputId = "";
				
				if(cellNameSuffix){
					currentInputId = this.GenerateCellName(i, j, cellNameSuffix);
				} else{
					currentInputId = this.GenerateCellName(i, j);
				}					
				
				var gridTextInputHtml = 
					'<input type="text" id="' + currentInputId +  '"class="textGrid" maxlength="1"> </input>'
				var currentRowIdTag = "#" + currentRowDivId;
				
				htmlString = htmlString.concat(gridTextInputHtml);				
			}
			
			var rowDivEndTag = '</div>';
			htmlString = htmlString.concat(rowDivEndTag);
		}
		
		return htmlString;
	},

	GetCurrentlyFocusedCell: function(){
		
		//TODO: Add type checking to ensure we get the cell
		var currentFocusedCell = $(':focus');

		return currentFocusedCell[0];		
	},
	
	GetCurrentlyFocusedCellJqueryTag: function(){
		
		var currentFocusedCell = GridManager.GetCurrentlyFocusedCell();
		var currentCellJQueryTag = "";
		
		//in some cases such as on a backspace keypress, 
		//currentFocusedCell[0] is undefined.
		if(currentFocusedCell && ! currentFocusedCell[0]){
			currentCellJQueryTag = "#" + currentFocusedCell.id;
		} else if(currentFocusedCell){
			//This is the most common execution path
			currentCellJQueryTag = "#" + currentFocusedCell[0].id;
		}
		
		return currentCellJQueryTag;
		
	},	
	
	GetGridTotalArea: function(height, width){
		var totalGridArea = (height * width) + 1; // Add one b/c we are zero based
		
		return totalGridArea;
	},
		
	GridInputHandlerSetup: function(height, width){
		var inputs = this.getInputs();
		
		var focusInputIndex = 0;
		var inputToGrantFocus;
		
		//Event Handler for KeyUp Events.
		//Returns granted focusInputIndex
		var keyUpHandler = function(e){ 
			
			var isBackspaceKey = KeyEventHandler.isBackspaceKey(e);
			var isLeftArrow = KeyEventHandler.isLeftArrow(e);
			var isRightArrow = KeyEventHandler.isRightArrow(e);
			var isUpArrow = KeyEventHandler.isUpArrow(e);
			var isDownArrow = KeyEventHandler.isDownArrow(e);
			var isTabKey = KeyEventHandler.isTabKey(e);
			var isShiftTabKey = KeyEventHandler.isShiftTabKey(e);
			
			var totalGridArea = GridManager.GetGridTotalArea(height, width);
			
			if(isDownArrow){
				//move down a row, until we hit the bottom

				var bottomRowFirstCell = (width * height) - width;
				var bottomRowLastCell = totalGridArea;
				
				var bottomRowHoldsFocus = 
					focusInputIndex >= bottomRowFirstCell 
						&& focusInputIndex <= bottomRowLastCell;
				
				if(!bottomRowHoldsFocus){
					focusInputIndex = inputs.index(e.target) + width;
				}
				
			} else if(isUpArrow){
				
				var topRowFirstCell = 0;
				var topRowLastCell = width - 1;
				
				var topRowHoldsFocus = 
					focusInputIndex >= topRowFirstCell 
						&& focusInputIndex <= topRowLastCell;
				
				if(!topRowHoldsFocus){
					focusInputIndex = inputs.index(e.target) - height;
				}
				
			} else if(isRightArrow || isTabKey){
				
				//console.log("move one cell forward");
				
				if(isTabKey){
					e.preventDefault();
				}
				//move one cell forward				
				var isAtEndOfRow = (inputs.index(e.target)) % width === width - 1;
				
				if(!isAtEndOfRow){
					focusInputIndex = inputs.index(e.target) + 1;
				}
				
			} else if(isLeftArrow || isShiftTabKey || isBackspaceKey){
				//move one cell back
				//console.log("move one cell back");
				if(isShiftTabKey){
					e.preventDefault();
				}
				
				var isAtEndOfRow = (inputs.index(e.target) + 1) %  width === 1;
				
				if(!isAtEndOfRow){
					focusInputIndex = inputs.index(e.target) - 1;
				}
			}

			return focusInputIndex;
		};
		
		var keyDownHandler = function(e){
			
			console.log("e.which = " + e.which);
			var isHomeKey = KeyEventHandler.isHomeKey(e);
			var isEndKey = KeyEventHandler.isEndKey(e);
			
			var currentFocusedCellCoordinates = 
				GridManager.GetCurrentlyFocusedCellCoordinates();
			
			if(isHomeKey || isEndKey){
				
				// in the default action, 
				//it just advances (destructively!!!) to the next cell.
				e.preventDefault(); 
				
				var yCoordinateToFocus;

				if(isHomeKey){
					//Move to the beginning of the row
					var firstCellOfRowIndex = 0;
					yCoordinateToFocus = firstCellOfRowIndex;
				} else {
					//Move to the end of the row
					var lastCellOfRowIndex = width - 1;
					yCoordinateToFocus = lastCellOfRowIndex;
				}
				
				var cellIdToGrantFocus = GridManager.GenerateCellName(currentFocusedCellCoordinates.XCoord, yCoordinateToFocus);
				var jQueryTagToGrantFocus = '#' + cellIdToGrantFocus
				
				$(jQueryTagToGrantFocus).focus();
				e.preventDefault();
			}
		};
		
		var keypressHandler = function(e){
			
			var isCurrentPressAdvancementKey = KeyEventHandler.IsAdvancementKey(e);
			
			if(!isCurrentPressAdvancementKey){ // These will be handled in the keyUpHandler
				//Note: In Firefox, shift tab works with no special handling
				//move to the next cell
				
				var currentFocusedCell = $(':focus');
				
				if(currentFocusedCell[0]){
					
					var currentCellJQueryTag = "#" + currentFocusedCell[0].id;					
					$(currentCellJQueryTag).val(e.char);
				}
				
				focusInputIndex = inputs.index(e.target) + 1;
				
			} else {
				//console.log("calling keyup handler");
				focusInputIndex = keyUpHandler(e);
			}
			
			return focusInputIndex;
		};
		
		var keypressHandlerWithAdvancement = function(e){

			focusInputIndex = keypressHandler(e)
			focusInputIndex = GridManager.handleCellAdvancement(focusInputIndex, height, width);						
		};
		
		var browserName = BrowserDetector.GetBrowserName();
		
		switch (browserName) {
		
		  case BrowserDetector.FIREFOX_NAME:
				inputs.keypress(keypressHandlerWithAdvancement);
				inputs.keydown(keyDownHandler);
				
			break;
			
			case BrowserDetector.CHROME_NAME:
				inputs.keyup(
					function(e){
												
						//var isKeyboardPasteEvent = KeyEventHandler.isKeyboardPaste(e);
						//console.log("In Chrome Keyup isKeyboardPasteEvent = " + isKeyboardPasteEvent);
						//console.log("In Chrome Keyup e = " + JSON .stringify(e));
						
						var isHomeOrEndKey = KeyEventHandler.isHomeOrEndKey(e);
						
						console.log("e.ctrlKey = " + e.ctrlKey);
						
						if(!KeyEventHandler.isTabOrShiftTab(e) && !isHomeOrEndKey && !e.ctrlKey){
						
							e.preventDefault();
							var focusInputIndex = keyUpHandler(e);
							console.log("Before call to GridManager.handleCellAdvancement, focusInputIndex = " +  focusInputIndex);
							focusInputIndex = GridManager.handleCellAdvancement(focusInputIndex, height, width);	
							
							console.log("In Chrome after handleCellAdvancement focusInputIndex = " + focusInputIndex);
						}else if(isHomeOrEndKey){
							keyDownHandler(e);
						}
						
					});
					
				inputs.keypress(keypressHandlerWithAdvancement);
				
			break;
			
			case BrowserDetector.IE_NAME:
				
				inputs.keypress(
				function(e){
					keypressHandlerWithAdvancement(e);
					return false;
				});
				
				inputs.keydown(
					function(e){
						if(KeyEventHandler.isHomeKey(e) || KeyEventHandler.isEndKey(e)){
							keyDownHandler(e);
						} else {	
						
							e.preventDefault();
							keypressHandlerWithAdvancement(e);
							return false;
						}
					});
			break;
			
			case BrowserDetector.EDGE_NAME:
				inputs.keyup(
					function(e){
						
						if(KeyEventHandler.isHomeKey(e) || KeyEventHandler.isEndKey(e)){
							keyDownHandler(e);
						} else {						
						
							e.preventDefault();
							var focusInputIndex = keyUpHandler(e);
					
							GridManager.handleCellAdvancement(focusInputIndex, height, width);
							return false;
						}
					});

				inputs.keypress(
					function(e){
						e.preventDefault();
						keypressHandlerWithAdvancement(e);
						return false;
					});
				
			break;
		}
	},
	
	//Handles the advancement of text across each cell.
	//Returns the index of which input should hold focus.
	handleCellAdvancement: function(focusInputIndex, height, width){
				
		//GridManager.
		//console.log("LastFocusedCellId = " + GridManager.LastFocusedCellId);
		
		var lastFocusedCoordinates = GridManager.GetCoordinatesFromCellId(GridManager.LastFocusedCellId);
		//focus wrap backwards ie, start populating the very last box
		
		//console.log("Handling Cell Advancement");
		
		//var isHomeOrEndKey = KeyEventHandler.isHomeOrEndKey(keyboardEvent);
		
		var gridArea = height * width;
		// zero based counting, so we need to remove one from the gridArea
		var lastAvailableCellIndex = gridArea - 1; 
		
		//console.log("Before compare focusInputIndex = " + focusInputIndex);
		
		if(focusInputIndex > lastAvailableCellIndex){
			focusInputIndex = lastAvailableCellIndex;
		}
		
		//console.log("After compare focusInputIndex = " + focusInputIndex);
		
		var inputs = GridManager.getInputs();
		var inputToGrantFocus = inputs.get(focusInputIndex);
		//console.log("inputToGrantFocus = " + JSON.stringify(inputToGrantFocus));
		
		if (inputToGrantFocus && !inputToGrantFocus[0]) {
			inputToGrantFocus.focus(); // So this would seem the rational thing to do.  
		} 
		else if(inputToGrantFocus[0]){
			inputToGrantFocus[0].focus();
		}
	  
	  return focusInputIndex;
	},
	
	getAllGridText: function(height, width){
		
		var inputs = this.getInputs();
		var inputsCount = this.getInputsCount();
		var gridText = "";
		
		for(i=0; i<inputsCount; i++){
			var currentCellTag = this.getCellTagFromInput(inputs[i]);
			
			var currentTagValue = $(currentCellTag).val();
			
			if(i > 0 && i % width == 0){
				//If we have ran out to the end of the line, move to the next line
				gridText += Constants.CARRIAGE_RETURN_NEW_LINE;
				//console.log("Adding new line to exported grid text");
			}
			
			if(currentTagValue){
				gridText += $(currentCellTag).val();
			} else{
				gridText += Constants.SPACE;
			}
		}
		
		return gridText;
	},
	
	getCellTagFromInput: function(inputObject){
		return "#" + inputObject.id;
	},
	
	getInputs: function(gridDivJQUeryTag){
		
		var inputsArray;
		
		if(gridDivJQUeryTag){
			//If they provide an arg use it.
			inputsArray = $(gridDivJQUeryTag + ' :input');
		} else{
			//otherwise, use the primary
			inputsArray = $(this.PrimaryGridDivToFilJQueryTag + ' :input');
		}
		
		return inputsArray;
	},
	
	getInputsCount: function(){
		return this.getInputs().length;
	},

	GenerateCellName: function(xCoord, yCoord, cellNameSuffix){
		
		var cellName = "x" + xCoord + "y" + yCoord + "Cell";
		
		if(cellNameSuffix){
			cellName = cellName.concat(cellNameSuffix);
		}
		
		return cellName;
	},

	GetCoordinatesFromCellId: function(cellId){
		
		console.log("cellId = " + cellId);
		
		//find the y
		var yCoordDelimiterIndex = cellId.indexOf("y");
		
		//get text between the x and y, but cut off the #x
		
		var xCoord;
		
		console.log("yCoordDelimiterIndex = " + yCoordDelimiterIndex);
		
		if(cellId.startsWith("#")){
			//Handle the start of a jquery Tag
			xCoord = cellId.slice(2, yCoordDelimiterIndex);
			//console.log("Handle the start of a jquery Tag xCoord = " + String(xCoord));
		} else {
			
			xCoord = cellId.slice(1, yCoordDelimiterIndex);
			//console.log("Handle the start of a non-JQuery Tag xCoord = " + String(xCoord));
		}			
		
		//Get the text between y and Cell
		var cellSuffixStartIndex = cellId.indexOf("Cell")
		var yCoord = cellId.slice(yCoordDelimiterIndex + 1, cellSuffixStartIndex);
		
		//console.log("yCoord = ", yCoord);
		
		var coordinates = new Object();
		coordinates.XCoord = parseInt(xCoord);
		coordinates.YCoord = parseInt(yCoord);
		
		if(!coordinates.XCoord){
			coordinates.XCoord = 0;
		}
		
		if(!coordinates.YCoord){
			coordinates.YCoord = 0;
		}
		
		return coordinates;
	},
	
	GetCurrentlyFocusedCellCoordinates: function(){
		var currentlyFocusedCellJQueryTag = GridManager.GetCurrentlyFocusedCellJqueryTag();
		var currentlyFocusedCoordinates = GridManager.GetCoordinatesFromCellId(currentlyFocusedCellJQueryTag);
		
		return currentlyFocusedCoordinates;
	},
	
	TrackFocus: function(cell){
		this.LastFocusedCellId = cell.id;
	},
}
