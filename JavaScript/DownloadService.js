
var DownloadService = {

	//Build the text file from the data on the grid, 
	//so the user can download it as a text file.
	DownloadGridData : function(){
		var downloadAnchor = window.document.createElement('a');
		
		var gridTextDump = GridManager.getAllGridText(gridHeight, gridWidth);
		
		downloadAnchor.href = 
			window.URL.createObjectURL(new Blob([gridTextDump],
									   {type: 'text'}));
		downloadAnchor.download = 'ASCIIPlan.txt';

		// Append anchor to body.
		document.body.appendChild(downloadAnchor)
		downloadAnchor.click();

		// Remove anchor from body
		document.body.removeChild(downloadAnchor);
	}

}